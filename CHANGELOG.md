This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for org.gcube.data.transfer.data-transfer-library

## [v1.3.0] 2020-12-01
* Upgraded integration with security layer 
* Removed http [#20752]


## [v1.2.3] 2020-12-01

* Deletion method
* GetInfo method
* GetStream method

## [v1.2.2] 2020-07-16

### Fixes

- Integration with gcube distribution (boms 2.0.0)
- TransfererBuilder.getTransfererByHost now follows redirects
