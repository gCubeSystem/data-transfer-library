package org.gcube.data.transfer.library;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.HashMap;

import org.gcube.contentmanagement.blobstorage.transport.backend.RemoteBackendException;
import org.gcube.data.transfer.library.faults.DestinationNotSetException;
import org.gcube.data.transfer.library.faults.FailedTransferException;
import org.gcube.data.transfer.library.faults.HostingNodeNotFoundException;
import org.gcube.data.transfer.library.faults.InitializationException;
import org.gcube.data.transfer.library.faults.InvalidDestinationException;
import org.gcube.data.transfer.library.faults.InvalidSourceException;
import org.gcube.data.transfer.library.faults.ServiceNotFoundException;
import org.gcube.data.transfer.library.faults.SourceNotSetException;
import org.gcube.data.transfer.library.faults.UnreachableNodeException;
import org.gcube.data.transfer.library.utils.StorageUtils;
import org.gcube.data.transfer.model.Destination;
import org.gcube.data.transfer.model.DestinationClashPolicy;
import org.gcube.data.transfer.model.PluginInvocation;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class TransfererTest {

//	static String hostname="https://thredds.dev.d4science.org";
//	static String scope="/gcube/devsec/devVRE";
	static String hostname="https://thredds-pre-d4s.d4science.org";
	static String scope="/pred4s/preprod/preVRE";
	
//	static String nodeId="462b68c5-463f-4295-86da-37d6c0abc7ea";

	static DataTransferClient client;

	static String link="https://nexus.d4science.org/nexus/content/groups/gcube-releases-all/org/gcube/data/transfer/data-transfer-model/1.0.0-3.11.0-128236/data-transfer-model-1.0.0-3.11.0-128236-javadoc.jar";
	
	static File tempFile; 
	
	@BeforeClass
	public static void init() throws UnreachableNodeException, ServiceNotFoundException, HostingNodeNotFoundException, IOException{
		TokenSetter.set(scope);
		client=DataTransferClient.getInstanceByEndpoint(hostname);
		//		client=DataTransferClient.getInstanceByNodeId(nodeId);
		
		// Download file from mvn 
		InputStream in = new URL(link).openStream();
		Path file=Files.createTempFile("test", ".jar");
		Files.copy(in, file, StandardCopyOption.REPLACE_EXISTING);
		tempFile=file.toFile();
		assertTrue(tempFile.getTotalSpace()>0);
	}

	@Test
	public void localFile() throws InvalidSourceException, SourceNotSetException, FailedTransferException, InitializationException, InvalidDestinationException, DestinationNotSetException{
		for(int i=0;i<3;i++){
			String localFile=tempFile.getAbsolutePath();
			String transferredFileName="Mindless.mp3";
			Destination dest=new Destination(transferredFileName);
			dest.setOnExistingFileName(DestinationClashPolicy.REWRITE);
			TransferResult res=client.localFile(localFile,dest);
			String remotePath=res.getRemotePath();		
			Assert.assertEquals(transferredFileName, remotePath.substring(remotePath.lastIndexOf(File.separatorChar)+1));
			System.out.println(res);
		}
	}


	@Test
	public void httpUrl() throws MalformedURLException, InvalidSourceException, SourceNotSetException, FailedTransferException, InitializationException, InvalidDestinationException, DestinationNotSetException{
		
		System.out.println(client.httpSource(link,new Destination("Some import")));
	}


	@Test
	public void storage() throws InvalidSourceException, SourceNotSetException, FailedTransferException, InitializationException, RemoteBackendException, FileNotFoundException, InvalidDestinationException, DestinationNotSetException{
		TokenSetter.set(scope);
		String toUpload=tempFile.getAbsolutePath();
		String id=StorageUtils.putOntoStorage(new File(toUpload));
		Destination dest=new Destination("some/where","My Pdf.pdf");
		TransferResult res=client.storageId(id,dest);
		Assert.assertTrue(res.getRemotePath().contains(dest.getSubFolder()+File.separatorChar+dest.getDestinationFileName()));
		System.out.println(client.storageId(id,dest));
	}

	@Test(expected=InvalidSourceException.class)
	public void wrongStorage() throws InvalidSourceException, SourceNotSetException, FailedTransferException, InitializationException, InvalidDestinationException, DestinationNotSetException{
		System.out.println(client.storageId("13245780t",new Destination("my file")));
	}

	@Test(expected=InvalidSourceException.class)
	public void wrongLocal() throws InvalidSourceException, SourceNotSetException, FailedTransferException, InitializationException, InvalidDestinationException, DestinationNotSetException{
		String localFile="/home/fabio/Downloads/123045689.mp3";
		System.out.println(client.localFile(localFile,new Destination("12345")));
	}

	@Test(expected=InvalidSourceException.class)
	public void wrongUrl() throws MalformedURLException, InvalidSourceException, SourceNotSetException, FailedTransferException, InitializationException, InvalidDestinationException, DestinationNotSetException{
		String link="https://www.some.where.com/over/theRainbow.txt";
		System.out.println(client.httpSource(link,new Destination("oz")));
	}
	
	
	
	@Test
	public void decompressPlugin() throws InvalidSourceException, SourceNotSetException, FailedTransferException, InitializationException, InvalidDestinationException, DestinationNotSetException{
		HashMap<String,String> invocationParams=new HashMap<String,String>();
		invocationParams.put("SOURCE_ARCHIVE", PluginInvocation.DESTINATION_FILE_PATH);		
		client.localFile(tempFile, new Destination("MyJar"),Collections.singleton(new PluginInvocation("DECOMPRESS", invocationParams)));
	}
}
