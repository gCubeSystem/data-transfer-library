package org.gcube.data.transfer.library;

import java.net.MalformedURLException;
import java.net.URL;

import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.gcube.data.transfer.library.client.AuthorizationFilter;
import org.gcube.data.transfer.library.client.Client;
import org.gcube.data.transfer.library.faults.CommunicationException;
import org.gcube.data.transfer.library.faults.RemoteServiceException;
import org.gcube.data.transfer.library.faults.ServiceNotFoundException;
import org.gcube.data.transfer.model.Destination;
import org.gcube.data.transfer.model.DestinationClashPolicy;
import org.gcube.data.transfer.model.RemoteFileDescriptor;
import org.gcube.data.transfer.model.ServiceConstants;
import org.gcube.data.transfer.model.TransferCapabilities;
import org.gcube.data.transfer.model.TransferRequest;
import org.gcube.data.transfer.model.TransferTicket;
import org.gcube.data.transfer.model.TransferTicket.Status;
import org.gcube.data.transfer.model.options.HttpDownloadOptions;
import org.gcube.data.transfer.model.settings.HttpDownloadSettings;
import org.glassfish.jersey.client.ClientConfig;
import org.junit.BeforeClass;
import org.junit.Test;


public class TestClientCalls {

	
	static String scope="/gcube/devsec/devVRE";
	
//	static String scope="/pred4s/preprod/preVRE";
	static String hostname="https://thredds.dev.d4science.org";
//	static String hostname="https://geona-proto.d4science.org";
//	static String scope="/d4science.research-infrastructures.eu/D4OS/GeoNA-Prototype";
	static Client client;
	static long testRun=System.currentTimeMillis();
	
	@BeforeClass
	public static void init() throws ServiceNotFoundException{
		TokenSetter.set(scope);
//		String hostname=
//				new URL(SDIAbstractPlugin.management().build().getConfiguration().getByEngine(Engine.TH_ENGINE).get(0).getBaseEndpoint()).getHost();
		client=new Client(hostname);
	}
	
	
	@Test
	public void getCapabilties() throws CommunicationException{
		System.out.println(client.getCapabilties());
	}
	
//	@Test
//	public void list() throws RemoteServiceException, CommunicationException {
//		for(String persistanceID:client.getCapabilties().getAvailablePersistenceIds())
//			System.out.println(client.getInfo(persistanceID));
//	}
	
	
	@Test 
	public void doTheTransfer() throws MalformedURLException, RemoteServiceException{
		System.out.println("Transferred "+transfer());
	}
	
	private RemoteFileDescriptor transfer() throws RemoteServiceException, MalformedURLException {
		Destination dest=new Destination("readme.md");
		dest.setCreateSubfolders(true);
		dest.setSubFolder("test/testSub/"+testRun);
		dest.setOnExistingFileName(DestinationClashPolicy.ADD_SUFFIX);
		dest.setOnExistingSubFolder(DestinationClashPolicy.APPEND);
		TransferRequest request= new TransferRequest("", new HttpDownloadSettings(new URL("https://code-repo.d4science.org/gCubeSystem/data-transfer-library/raw/branch/master/README.md"), HttpDownloadOptions.DEFAULT),dest);
		System.out.println("Submitting "+request);
		TransferTicket ticket=client.submit(request);
		System.out.println("Ticket is "+ticket);	
		
		boolean continuePolling=true;
		do{
		ticket=client.getTransferStatus(ticket.getId());
		System.out.println("Status : "+ticket);
		continuePolling=ticket.getStatus().equals(Status.PENDING)||ticket.getStatus().equals(Status.TRANSFERRING)||ticket.getStatus().equals(Status.WAITING);
		try{
			Thread.sleep(1000);
		}catch(InterruptedException e){}
		}while(continuePolling);
		
		Destination result=ticket.getDestinationSettings();
		return client.getInfo(result.getPersistenceId()+"/"+dest.getSubFolder()+ticket.getDestinationFileName().substring(ticket.getDestinationFileName().lastIndexOf("/")));
	}
	

	@Test
	public void directCall(){		
		javax.ws.rs.client.Client client = ClientBuilder.newClient(new ClientConfig().register(AuthorizationFilter.class));
		WebTarget target=client.target(hostname+"/data-transfer-service"+ServiceConstants.APPLICATION_PATH+"Capabilities");
//		WebTarget target=client.target(hostname+"/data-transfer-service/gcube/service/Capabilities");
		System.out.println("Asking capabilities to target : "+target.getUri());
		System.out.println("RESPONSE AS STRING ");
		System.out.println(target.
				request(MediaType.APPLICATION_JSON_TYPE).get(String.class));
		System.out.println("RESPONSE AS OBJ ");
		TransferCapabilities cap=target.
				request(MediaType.APPLICATION_JSON_TYPE).get(TransferCapabilities.class);
		System.out.println(cap.getAvailablePersistenceIds());
	}
	
	
	@Test
	public void delete() throws RemoteServiceException, MalformedURLException {
		RemoteFileDescriptor remote=transfer();
		client.delete(remote);
		
	}
	
	@Test
	public void deleteFolder() throws RemoteServiceException, MalformedURLException {
		RemoteFileDescriptor remote=transfer();
		// try to delete parent
		client.delete(remote.getPersistenceId()+"/"+remote.getPath().substring(0,remote.getPath().lastIndexOf("/")));
	}
	
	@Test
	public void openStream() throws RemoteServiceException, MalformedURLException {
		RemoteFileDescriptor remote=transfer();
		client.getInputStream(remote);
		//try stream parent
		client.getInputStream(remote.getPersistenceId()+"/"+remote.getPath());
	}
	
	@Test 
	public void inspect() throws RemoteServiceException, MalformedURLException {
		transfer();
		RemoteFileDescriptor remote=transfer();
		System.out.println("Remote folder children are : "+client.getInfo(remote.getPersistenceId()+"/"+remote.getPath()));
		client.getInfo(remote);
	}
}
