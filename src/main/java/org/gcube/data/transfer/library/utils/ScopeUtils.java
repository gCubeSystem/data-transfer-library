package org.gcube.data.transfer.library.utils;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.scope.api.ScopeProvider;

import static org.gcube.common.authorization.client.Constants.authorizationService;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ScopeUtils {

	public static String getCurrentScope(){
		return ScopeProvider.instance.get();
	}
	
	
	public static String getCurrentCaller(){
		try{
			return AuthorizationProvider.instance.get().getClient().getId();
		}catch(Throwable e ){
			try {
				log.warn("Unable to get Client ID from UMA , checking gcube-token.. ",e);
				String token=SecurityTokenProvider.instance.get();
				return authorizationService().get(token).getClientInfo().getId();
			}catch(Throwable e1 ){
				log.warn("Unable to resolve gcube-token ",e1);
				return "Unidentified data-transfer user";
			}
			
			
		}
	}
}
