package org.gcube.data.transfer.library.faults;

public class CommunicationException extends DataTransferException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CommunicationException() {
		// TODO Auto-generated constructor stub
	}

	public CommunicationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CommunicationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CommunicationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CommunicationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
