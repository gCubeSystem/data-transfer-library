package org.gcube.data.transfer.library.faults;

public class InvalidDestinationException extends DataTransferException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1882334325516885722L;

	public InvalidDestinationException() {
		// TODO Auto-generated constructor stub
	}

	public InvalidDestinationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public InvalidDestinationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidDestinationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public InvalidDestinationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
